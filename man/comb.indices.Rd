% Generated by roxygen2: do not edit by hand
% Please edit documentation in R/comb.indices.R
\name{comb.indices}
\alias{comb.indices}
\title{Construction of selection indices based on number of character grouping}
\usage{
comb.indices(ncomb, phen_mat, gen_mat, weight_mat, weight_col = 1, GAY)
}
\arguments{
\item{ncomb}{Number of Characters/Traits group}

\item{phen_mat}{Phenotypic Variance-Covariance Matrix}

\item{gen_mat}{Genotypic Variance-Covariance Matrix}

\item{weight_mat}{Weight Matrix}

\item{weight_col}{Weight column number incase more than one weights, by default its 1}

\item{GAY}{Genetic Advance of comparative Character/Trait i.e. Yield (Optional argument)}
}
\value{
Data frame of all possible selection indices
}
\description{
Construction of selection indices based on number of character grouping
}
\examples{
gmat<- gen.varcov(seldata[,3:9], seldata[,2], seldata[,1])
pmat<- phen.varcov(seldata[,3:9], seldata[,2], seldata[,1])
comb.indices(ncomb = 1, phen_mat = pmat, gen_mat = gmat, weight_mat = weight[,-1], weight_col = 1)

}
